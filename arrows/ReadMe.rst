.. image:: ../doc/kwiver_Logo-300x78.png
   :alt: KWIVER

Directory Structure and Provided Functionality
==============================================

===================== =========================================================
`<arrows/ceres>`_     Algorithms for bundle adjustment and optimization using
                      `Ceres Solver`_.
`<arrows/core>`_      Algorithms implemented with no additional third party
                      dependencies beyond what Vital uses (Eigen).
`<arrows/super3d>`_   Depth estimation from video, uses VXL
`<arrows/ffmpeg>`_    Video support through the FFmpeg library.
`<arrows/mvg>`_       Multi-View Geometry algorithms with no additional
                      dependencies.
`<arrows/ocv>`_       Algorithms implemented using OpenCV_.
                      Includes feature detectors and descriptor, homography
                      and fundamental matrix estimation, image IO, and more.
`<arrows/proj>`_      Geographic conversion functions implemented with PROJ_.
`<arrows/uuid>`_      [*Experimental*] Implementation of unique IDs using libuuid
`<arrows/vxl>`_       Algorithms implemnted using the VXL_ libraries.
                      Includes bundle adjustment, homography estimation, video
                      file reading, and more.
===================== =========================================================
